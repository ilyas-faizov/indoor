import $ from "jquery";
import "bootstrap";
import "popper.js";
import "magnific-popup";
import "owl.carousel";
import "jquery-match-height";
import Parallax from "parallax-js";
import Masonry from "masonry-layout";
import Typed from 'typed.js';
import WOW from "wow.js";

$(window).on("load", function() {

	function loadKrug(){
		$('#loader-wrapper').find('#loader').fadeOut().end().delay(400).fadeOut('slow');
	}
	loadKrug();

	function typedFunc(){
    var typed = new Typed("#main-heading-plank-content", options);
  }
  
  if($("span").is("#main-heading-plank-content")){
    typedFunc();
  }

	/*$(window).scroll(function(){
    if ($(this).scrollTop()>500) {
			$('.header-fixed').addClass('show');
    } else {
      $('.header-fixed').removeClass('show');
    };
  });*/

  function mainHeading(){
  	if($("div").is(".main-heading")){
	  	var element1 = document.querySelector('.main-heading h1');
		  element1.classList.add('animated', 'fadeInLeftBig');
		}
	}
	function mainHeadingcontent(){
		if($("div").is(".main-heading-content")){
			var element2 = document.querySelector('.main-heading-content');
		  element2.classList.add('animated', 'fadeInLeftBig');
		}
	}
	function calculateOnline(){
		if($("a").is(".calculate-online")){
			var element3 = document.querySelector('.calculate-online');
		  element3.classList.add('animated', 'bounceIn');
		}
	}
	function orderAnad(){
		if($("a").is(".calculate-dark")){
			var element3 = document.querySelector('.calculate-dark');
		  element3.classList.add('animated', 'bounceIn');
		}
	}
	function timeImg(){
		if($("div").is(".time-img")){
			var element4 = document.querySelector('.time-img img');
		  element4.classList.add('animated', 'fadeInDown');
		}
	}

	function wowAnimation(){
		new WOW().init({
			mobile: false
		});
		mainHeading();
		mainHeadingcontent();
		calculateOnline();
		orderAnad();
		timeImg();
  }
	wowAnimation();
	
	function scrollUp(){
		var btn = $('#scroll-up');
		$(window).scroll(function() {
		  if ($(window).scrollTop() > 300) {
			btn.addClass('show');
		  } else {
			btn.removeClass('show');
		  }
		});

		btn.on('click', function(e) {
		  e.preventDefault();
		  $('html, body').animate({scrollTop:0}, '300');
		});
	}
	scrollUp();

	function parAllax(){
		var scene = document.getElementById('scene');
		var parallaxInstance = new Parallax(scene, {
		  relativeInput: true
		});
	}
	
	if($("div").is("#scene")){
		parAllax();
	}

	$('.parthners-logo__slider').owlCarousel({
    loop: true,
    margin: 30,
    nav: true,
    dots: false,
    autoWidth: true,
    items: 4,
    center:true,
    autoplay: true,
    autoplaySpeed: 3000,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsive:{
      0:{
        items:1
      },
      768:{
        items:2
      },
      992:{
        items:3
      },      
      1200:{
        items:4
      }
  	}
	});

	$('.popup-with-form').magnificPopup({
		type: 'inline',
		fixedContentPos: true,
		fixedBgPos: false,
		overflowY: 'auto',
		closeBtnInside: true,
		tClose: 'Закрыть (Esc)',
  	tLoading: 'Загрузка...',
		preloader: true,
		midClick: false,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in',
		callbacks: {
			open: function() {
				var e=$.magnificPopup.instance, t=$(e.currItem.el[0]);
				$(".form-title h3").text(t.data("name")),
				$(".form-input").val(t.data("name"))
			}
		}
	});

	$('.popup-gallery').each(function() {
		$(this).magnificPopup({
			delegate: 'a',
			type: 'image',
			tClose: 'Закрыть (Esc)',
			tLoading: 'Загрузка изображения #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1], // Will preload 0 - before current, and 1 after the current image
				tPrev: 'Предыдущая (Стрелка влево)', // Alt text on left arrow
		    tNext: 'Следующая (Стрелка вправо)', // Alt text on right arrow
		    tCounter: '%curr% из %total%' // Markup for "1 of 7" counter
			}
		});
	});

	$('.portfolio-slider').owlCarousel({
    loop: true,
    nav: true,
    dots: false,
    items: 1,
    //autoplay: true,
    autoplaySpeed: 3000,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
	});

	$('.header-menu').on('click', function(){
  	var el = $(this);
      el.toggleClass('show');
      if (el.hasClass('show')) {
        el.find('.hide-menu').removeClass('invisible');
      } else {
      	setTimeout(function(){
        	el.find('.hide-menu').addClass('invisible');
        }, 300);
      }
  });

  $('.custom-checkbox').click(function(){
    var checkbox = $(this).find('input[type=checkbox]');
    // если чекбокс был активен
    if(checkbox.prop("checked")){
      // снимаем класс с родительского дива
      $(this).removeClass("active");
      // и снимаем галочку с чекбокса
      checkbox.prop("checked", false);
  // если чекбокс не был активен
    }else{
      // добавляем класс родительскому диву
      $(this).addClass("active");
      // ставим галочку в чекбоксе
      checkbox.prop("checked", true);
    }
  });

  function imgSvg(){
		$('img.img-svg').each(function(){
		  var $img = $(this);
		  var imgClass = $img.attr('class');
		  var imgURL = $img.attr('src');
		  $.get(imgURL, function(data) {
		    var $svg = $(data).find('svg');
		    if(typeof imgClass !== 'undefined') {
		      $svg = $svg.attr('class', imgClass+' replaced-svg');
		    }
		    $svg = $svg.removeAttr('xmlns:a');
		    if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
		      $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
		    }
		    $img.replaceWith($svg);
		  }, 'xml');
		});
	}
	imgSvg();

	function masOnry(){
		var elem = document.querySelector('.grid');
		var msnry = new Masonry( elem, {
		  // options
		  itemSelector: '.grid-item',
		  horizontalOrder: false,
		  stagger: 30
		  //columnWidth: 200
		});
  }
  if($("div").is(".project-block")){
		masOnry();
	}

	function mHeight(){
		$(".advantages-title").matchHeight();
		$(".advantages-text").matchHeight();
		$(".services-card__item").matchHeight();
		$(".services-card__img").matchHeight();
		$(".services-card__title").matchHeight();
		$(".form-check-mh").matchHeight({
			byRow: false
		});
	}
	mHeight();

	$(window).on("resize", function(){
	  //alert('Размеры окна браузера изменены.');
	  loadKrug();
	  mHeight();
	});
});

