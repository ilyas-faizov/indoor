$(document).ready(function(){
	
// 	Scroll button
	var btn = $('#scroll-up');

	$(window).scroll(function() {
	  if ($(window).scrollTop() > 300) {
		btn.addClass('show');
	  } else {
		btn.removeClass('show');
	  }
	});

	btn.on('click', function(e) {
	  e.preventDefault();
	  $('html, body').animate({scrollTop:0}, '300');
	});
	
    $('.header-menu').on('click', function(){
    	var el = $(this);
        el.toggleClass('show');
        if (el.hasClass('show')) {
	        el.find('.hide-menu').removeClass('cssinvisible');
        } else {
        	setTimeout(function(){
	        	el.find('.hide-menu').addClass('cssinvisible');
	        }, 300);
        }
    });

	$(".a-w5-image").each(function(){
		var cssBg = $(this).attr("data-bg");
		$(this).css("background-image","url('"+cssBg+"')");
	});
	
	$('.owl-carousel.m-window4-slider').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:true,
	    autoplay:true,
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});
});